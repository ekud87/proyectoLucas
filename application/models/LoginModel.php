<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function loguearse($data){
		$this->db->where('usuario', $data['usuario']);
		$this->db->where('contrasenia', $data['contrasenia']);
		$query = $this->db->get('usuarios');
		if($query->num_rows() > 0){
			return $query;
		} else{
			return false;
		}
		
	}
}

?>