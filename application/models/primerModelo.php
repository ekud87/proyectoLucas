<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PrimerModelo extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function crearRegistro($data){
		$this->db->insert('primera_tabla',
			array(
				'nombre' => $data['nombre'],
				'cantidad' => $data['cantidad']
			)
		);
	}
}

?>