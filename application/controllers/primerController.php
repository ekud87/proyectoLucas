<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PrimerController extends CI_Controller {
	function __construct(){
		parent::__construct();
		
		// Carga helpers
		$this->load->helper('primerHelper');
		$this->load->helper('form');
		
		// Carga modelo
		$this->load->model('primerModelo');
		
	}

	function index(){
		
		// Carga Librerias
		$this->load->library('primeralibreria', array('Inicio', 'Contacto', 'Cursos'));
		$data['mi_menu'] = $this->primeralibreria->construirMenu();
		
		// Envio de datos:
		$data['cadena_1'] = 'prueba envio dato';
		$data['cadena_2'] = 'prueba envio dato 1';
		$this->load->view('primerControlador/primerVista', $data);
	}
	
	//http://localhost/CodeIgniter-3.1.10/index.php/primerController/cualquiera
	function cualquiera(){
		$this->load->view('headers');
		$this->load->view('primerControlador/segundaVista');
	}
	
	//http://localhost/CodeIgniter-3.1.10/index.php/primerController/cualquiera
	function formHelper(){
		$this->load->view('headers');
		$this->load->view('primerControlador/terceraVista');
	}
	
	function recibirDatos(){
		$data = array(
			'nombre' => $this->input->post('nombre'),
			'cantidad' => $this->input->post('cantidad')
		);
		$this->primerModelo->crearRegistro($data);
		
		$this->load->view('headers');
		$this->load->view('primerControlador/terceraVista');
	}
}
?>