<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
	function __construct(){
		parent::__construct();
		
		// Carga helpers
		$this->load->helper('form');
		
		// Carga modelo
		$this->load->model('LoginModel');
	}

	function index(){
		$data['url'] = base_url().'application/views/plantilla/';
	    $this->load->view('sistemaLogin/login', $data);
	}
		
	function loguearse(){
		$data = array(
			'usuario' => $this->input->post('usuario'),
			'contrasenia' => $this->input->post('contrasenia')
		);
		$usuario = $this->LoginModel->loguearse($data);
		$data['url'] = URL_PROYECTO.'plantilla/';

		if($usuario){
			$this->load->view('headers');
			$this->load->view('plantilla/index', $data);
		}else{
		    $this->load->view('sistemaLogin/login', $data);
		}
	}
}
?>