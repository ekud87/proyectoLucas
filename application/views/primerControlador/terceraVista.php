<?= form_open("primerController/recibirDatos") ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');
	$nombre = array(
		'name' => 'nombre',
		'placeholder' => 'Escribe tu nombre'
	);
	$cantidad = array(
		'name' => 'cantidad',
		'placeholder' => 'Ingrese una cantidad '
	);
?>
<?= form_label('Nombre: ', 'nombre') ?>
<?= form_input($nombre) ?>
</br></br>
<?= form_label('Cantidad: ', 'cantidad') ?>
<?= form_input($cantidad) ?>
</br></br></br>

<?= form_submit('','Ejecutar Form') ?>
<?= form_close() ?>

</body>
</html>